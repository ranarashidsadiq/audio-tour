import fonts from './fonts';
import metrics from './metrics';
import colors from './colors';

const Styles = {
  screen: {
    mainContainer: {
      flex: 1,
      backgroundColor: colors.white,
    },
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
    title: {
      color: colors.snow,
    },
    topTitle:{
      fontSize: fonts.size.regular,
      width: metrics.deviceWidth * 2 / 3,
      marginLeft: metrics.baseMargin,
      paddingLeft: metrics.baseMargin,
      textAlign: 'center',
      color: colors.appGreen
    },

    subtitle: {
      color: colors.snow,
    },
    overlay: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    flexDirection: 'column',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  end: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  spaceBetween: {
    justifyContent: 'space-between',
  },
  smallCircle: {
    height: metrics.icons.small * 2/3,
    width: metrics.icons.small * 2/3,
    borderRadius: (metrics.icons.small * 2/3 ) /2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mediumCircle: {
    height: metrics.deviceWidth/12,
    width: metrics.deviceWidth/12,
    borderRadius: metrics.icons.small/2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  largeCircle: {
    height: metrics.icons.large,
    width: metrics.icons.large,
    borderRadius: metrics.icons.large/2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  xlCircle: {
    height: metrics.icons.xl,
    width: metrics.icons.xl,
    borderRadius: metrics.icons.xl/2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContain: {
    resizeMode: 'contain',
    flex: 1,
  },
  imageContainFlex: {
    resizeMode: 'contain',
    flex: 1,
  },
  imageCover: {
    resizeMode: 'cover',
    flex: 1
  },
  wrap: {
    alignItems: 'center',
    flexDirection:'row' ,
    flexWrap: 'wrap'
  },
};

export default Styles;
