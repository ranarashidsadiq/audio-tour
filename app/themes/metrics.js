import { Dimensions, Platform } from 'react-native';

const { width, height } = Dimensions.get('window');

const deviceWidth = width < height ? width : height;
const deviceHeight = width < height ? height : width;

// Dynamique value for responsive elemenet ( margin , Radius , sizeImage .....)
const metrics = {
  agendaDutchText: 'Blijf op de hoogte. Bekijk onze agenda en mis niets! We organiseren markten, geven extra aandacht aan speciale dagen zoals vaderdag, moederdag en oud & nieuw. Én we vinden het leuk om af en toe iets bijzonders te organiseren om jouw culinaire zintuigen weer een boost te geven.',
  workshopDutchText: 'Workshops, proeverijen en excursies. Voor jong en oud. Je kunt natuurlijk heerlijk shoppen in de Proeffabriek. Maar naast fantastische winkels bieden wij ook super leuke workshops, proeverijen en excursies aan. Pas dan komt de Proeffabriek echt tot leven. Hieronder geven we een opsomming van al onze arrangementen. Kom je er niet uit? Of wil je arrangementen combineren? Geef ons een seintje. We vertellen je graag over de mogelijkheden.',
  
  deviceWidth,
  deviceHeight,
  base: 36,
  baseMargin: 12,
  contactScreenMargin: 48,
  doubleBaseMargin: 24,
  smallMargin: 6,
  basePadding: 12,
  selectLanguageMargin: 88,
  doubleBasePadding: 24,
  smallPadding: 6,

  navBarHeight: (Platform.OS === 'ios') ? 64 : 54,
  radius: 5,
  buttonHeight: 45,
  inputHeight: 45,
  borderWidth: 1,
  rowHeight: 100,
  icons: {
    tiny: 15,
    small: 35,
    small_25: 25,
    medium: 45,
    lm: 72,
    large: 120,
    xl: 180,
  },
  images: {
    tiny: 12,
    small: 35,
    medium: 40,
    large: 90,
    xl: 120,
    logo: 180,
    flex: 360,
  },
  button: {
    small: 120,
    medium: 180,
    large: 240,
  }
};

export default metrics;
