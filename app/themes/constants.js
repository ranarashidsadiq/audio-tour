import { Dimensions, Platform } from 'react-native';

const constants = {
  agendaDutchText : 'Blijf op de hoogte. Bekijk onze agenda en mis niets! We organiseren markten, geven extra aandacht aan speciale dagen zoals vaderdag, moederdag en oud & nieuw. Én we vinden het leuk om af en toe iets bijzonders te organiseren om jouw culinaire zintuigen weer een boost te geven.'
};


export default constants;
