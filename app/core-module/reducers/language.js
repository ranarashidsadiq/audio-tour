import {
    DUTCH,
    BRABANT
} from '../actions/type';
const initialState = { isDutch: true };

const language = (state = initialState, action) => {
    switch (action.type) {
        case BRABANT:
            return { ...state, isDutch: false };
        case DUTCH:
            return { ...state, isDutch: true };
        default:
            return state;
    }
    return state;
};

export default language;
