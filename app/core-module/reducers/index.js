import { combineReducers } from 'redux'
import doctors from './doctors'
import language from "./language"

const doctorApp = combineReducers({
    doctors,
    language
})

export default doctorApp