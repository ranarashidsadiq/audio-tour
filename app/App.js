import React, { Component } from 'react'
import { AsyncStorage } from "react-native";
import { Provider, connect } from 'react-redux'
import { persistStore } from "redux-persist";
import { addNavigationHelpers } from "react-navigation";
// import { createStore } from 'redux'

import configureStore from "./redux/create";
import AppNavigator from "./AppNavigator";

const mapStateToProps = state => ({
    nav: state.nav
});



import doctorsApp from './core-module/reducers'
import AudioList from './containers/AudioList'
import IntroScreen from './containers/IntroScreen'
import MenuScreen from './containers/MenuScreen'
import ContactScreen from './containers/ContactScreen'
import InfoScreen from './containers/InfoScreen'

// const store = createStore(doctorsApp)

const App = connect(mapStateToProps)(({ dispatch, nav }) => (
    <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
));

const store = configureStore(doctorsApp);

class ReduxApp extends React.Component {
    componentDidMount() {
        persistStore(store, { storage: AsyncStorage });
    }

    render() {
        return (
            <Provider store={store}>
                <AppNavigator />
            </Provider>
        );
    }
}

export default ReduxApp;