import React, { Component } from 'react'
import { ListView, Modal, Text, TouchableOpacity, View, Image } from 'react-native'

import AudioCelitem from '../components/AudioCelitem'

import MenuScreen from "../containers/MenuScreen";

import TopBar from "../components/TopBar";

import styles from './styles/doctors';

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showMenu: false
        }
    }

    menuDrawer = () => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    };

    render() {

        return (
            <View style={styles.container}>
                <View>
                    <TopBar showBack={false} showLogo={true} showMenu={true} onClick={this.menuDrawer} />
                </View>
                {/* <View style={styles.toolbar}>
                    <Image style={styles.logo}
                        source={require('./images/lg.png')} />

                    <TouchableOpacity onPress={this.menuDrawer}>
                        <View style={styles.toolbarItem}>
                            <Image style={styles.menu1} source={require('./images/menu.png')} />
                        </View>
                    </TouchableOpacity>
                </View> */}
                <ListView
                    contentContainerStyle={styles.grid}
                    dataSource={this.props.doctorsDataSource}
                    renderRow={
                        (doctor) => {
                            return (
                                <AudioCelitem
                                    doctor={doctor}
                                    doctorList={this.props.doctorsDataSource}
                                    onSelectDoctor={this.props.selectDoctor}
                                    language={this.props.language}
                                />
                            )
                        }
                    }
                />

                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.showMenu}
                    onRequestClose={this.props.showDoctorslist}
                >
                    <MenuScreen menuDrawer={this.menuDrawer} colorOrange={true} />
                </Modal>
            </View>
        )
    }
}

export default Home