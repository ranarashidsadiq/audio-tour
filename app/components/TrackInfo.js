import React, { PureComponent } from 'react';
import { Dimensions, Modal, View, StyleSheet, Text, Image, TouchableOpacity, Platform } from 'react-native';

import KeepAwake from 'react-native-keep-awake';

import Sound from "react-native-sound";

import MenuScreen from "../containers/MenuScreen";

import Icon from "react-native-vector-icons/MaterialIcons";

import Slider from "react-native-slider";

import Carousel, { Pagination } from 'react-native-snap-carousel';

import TopBar from "../components/TopBar"
import { colors, metrics } from '../themes';
import SideSwipe from 'react-native-sideswipe';

const doctorsData = require('../core-module/data/doctors.json');

// var audioTime;
// var whoosh;
let deviceWidth = Dimensions.get('window').width;
class TrackInfo extends PureComponent {

    

    constructor(props) {
        super(props);

        this.state = {
            // showMenu: false,
            trackActive: false,
            actorName: "",
            volume: 0.5,
            seekBar: 0.00,
            mute: false,
            loadCarousel: true,
            trackDuration: 0.0,
            totalMinutes: 0,
            totalSeconds: 0,
            remainingMinutes: 0,
            remainingSeconds: 0,
            showColonWithZero: ":",
            showColonWithSeconds: ":0",
            img: "",
            trackNumber: -1,
            loadFirst: false,
            entries: doctorsData,
            whoosh : null,
            audioTime : null,
            showNextTrackButton: true,
            showPrevTrackButton: true
        }
    }

    componentWillMount() {
        // let name = this.props.navigation.state.params.doctor.actor
        // name = name.toUpperCase();
        // this.setState({
        //     actorName: name
        // });
        this.setState ({
            whoosh : this.props.whoosh,
            audioTime : this.props.audioTime
        })
        const index = this.props.currentindex;
        if (index === 0) {
            this.setState({
                showPrevTrackButton: false
            })
        }

        if (index === 23) {
            this.setState({
                showNextTrackButton: false
            })
        }
        this.setState({
            whoosh: this.props.whoosh,
            audioTime: this.props.audioTime
        })
    }
    loadTrack = (trackIndex) => {
        if (trackIndex === 0) {
            this.setState({
                showPrevTrackButton: false
            })
        } else {
            this.setState({
                showPrevTrackButton: true
            })
        }
        if (trackIndex === 23) {
            this.setState({
                showNextTrackButton: false
            })
        } else {
            this.setState({
                showNextTrackButton: true
            })
        }
        var doctor = doctorsData[trackIndex]
        var track = this.props.language.isDutch ? doctor.audioDutch : doctor.audioBrabants
        this.makeTrack(track)
        this.setState({ trackNumber: trackIndex, trackActive: true, actorName: doctor.actor.toUpperCase() })
        
    }
    makeTrack = (track) => {
        // alert('t')
        
        if (this.state.whoosh != null) {
            
            this.state.whoosh.stop();
            if (this.state.audioTime != null) {
                clearInterval(this.state.audioTime);
            }
        }
        this.setState({
            whoosh : new Sound(track, Sound.MAIN_BUNDLE, (error) => {
                this.state.whoosh.setCategory('Playback')
            if (error) {
                console.log('failed to load the sound', error);
                return;
            }
            // loaded successfully
            // setTimeout(() => {
                this.state.whoosh.play();
            // }, 1000);
            
            if (this.state.mute == true){
                this.setState({
                    volume: 0
                });
            }
            this.state.whoosh.setVolume(this.state.volume);

            setTimeout(() => {
                this.setState({
                    trackDuration: this.state.whoosh.getDuration()
                });
    
                var mind = this.state.whoosh.getDuration() % (60 * 60);
                var minutes = Math.floor(mind / 60);
    
                var secd = mind % 60;
                var seconds = Math.ceil(secd);
    
                if (seconds < 10) {
                    this.setState({
                        showColonWithZero: ":0"
                    });
                } else {
                    this.setState({
                        showColonWithZero: ":"
                    });
                }
    
                this.setState({
                    totalMinutes: minutes,
                    totalSeconds: seconds
                })
                this.state.audioTime = setInterval(this.secToMin, 500);
            }, 300);

            

            //alert('duration in seconds: ' + whoosh.getNumberOfChannels());
        })
    });
    };


    componentDidMount() {

        this.setState({
            trackActive: true
        });
        // this.setState({ trackNumber: 0 })
        // setTimeout(() => {
        //     this.setState({loadFirst : true})
        // }, 600);
        // var doctor = doctorsData[0]

        // var track = this.props.navigation.state.params.language.isDutch ? doctor.audioDutch : doctor.audioBrabants
        // this.makeTrack(track)
        this.setState({actorName: this.props.doctor.actor.toUpperCase() })
        this.makeTrack(this.props.track)

    }

    componentWillUnmount() {
        
        this.state.whoosh.stop();
        // this.state.whoosh = null;
        KeepAwake.deactivate();
        clearInterval(this.state.audioTime);
        // this.state.audioTime = null
    }

    playPause = () => {
        setTimeout(() => {
            this.state.trackActive ? this.state.whoosh.pause() : this.state.whoosh.play();
        this.setState({
            trackActive: !this.state.trackActive
        });
        }, 300);
        
    }

    fastForwardTrack = () => {
        let forward = this.state.seekBar
        forward = forward + 8;
        this.state.whoosh.setCurrentTime(forward);
    }

    forwardTrack = () => {
        let forward = this.state.seekBar
        forward = forward + 10;
        this.state.whoosh.setCurrentTime(forward);
    }

    fastRewindTrack = () => {
        let rewind = this.state.seekBar
        rewind = rewind - 8;
        this.state.whoosh.setCurrentTime(rewind);
    }

    rewindTrack = () => {
        let rewind = this.state.seekBar
        rewind = rewind - 10;
        this.state.whoosh.setCurrentTime(rewind);
    }

    secToMin = () => {

        this.state.whoosh.getCurrentTime((seconds) => {
            var mind = seconds % (60 * 60);
            var trackMinutes = Math.floor(mind / 60);

            var secd = mind % 60;
            var trackSeconds = Math.ceil(secd);

            if (trackSeconds < 10) {
                if (this.refs.timetext) {
                    this.setState({
                        showColonWithSeconds: ":0"
                    });
                }
                
                
            } else {
                if (this.refs.timetext) {
                    this.setState({
                        showColonWithSeconds: ":"
                    });
                }
                
            }
            // console.log("trackMinutes" + trackMinutes);
            
            this.setState({
                seekBar: seconds,
                remainingMinutes: trackMinutes,
                remainingSeconds: trackSeconds
            });
            console.log("trackSeconds:" + this.state.remainingSeconds);

        });

        if (this.state.remainingMinutes == this.state.totalMinutes && this.state.remainingSeconds == this.state.totalSeconds) {
            this.setState({
                trackActive: false,
            });
            this.state.whoosh.setCurrentTime(0);
            this.state.whoosh.stop();
        }

    }

    controlVolume = (volume) => {
        this.state.whoosh.setVolume(volume);
        if (volume == 0) {
            this.setState({
                mute: true
            });
        } else {
            this.setState({
                mute: false
            });
        }
        this.setState({
            volume: volume
        });

    }

    controlTrack = (seekBar) => {
        this.setState({
            seekBar: seekBar
        });
        this.state.whoosh.setCurrentTime(seekBar);
    }


    mute = () => {
        if (!this.state.mute) {
            this.setState({
                mute: true
            });
            this.state.whoosh.setVolume(0);
        } else {
            this.setState({
                mute: false
            });
            this.state.whoosh.setVolume(this.state.volume);
        }

    }
    switchPrev = () => {
        
    }
    switchNext = () => {
        
    }

    render() {
        return (
            <View>
                {(this.state.showPrevTrackButton && Platform.OS === 'android') ? <Icon
                    style={{ position: "absolute", marginTop: -(metrics.deviceHeight * (0.40)) / 1.7, left: -10 }}
                    // onPress={this.switchPrev}
                    name="chevron-left"
                    size={metrics.icons.medium}
                    color={colors.white}
                /> : <View></View>}
                {(this.state.showNextTrackButton && Platform.OS === 'android') ? <Icon
                    style={{ position: "absolute", marginTop: -(metrics.deviceHeight * (0.40)) / 1.7, right: -10 }}
                    // onPress={this.switchNext}
                    name="chevron-right"
                    size={metrics.icons.medium}
                    color={colors.white}
                /> : <View></View>}

                <View style={{ marginHorizontal: 12, justifyContent: "center" }}>
                    <Text style={{ marginTop: 0, color: "white" }}>{this.state.actorName}</Text>
                    <Text style={{ marginTop: 0, color: "white" }}>{this.state.totalMinutes + this.state.showColonWithZero + this.state.totalSeconds}</Text>
                    <Slider
                        value={this.state.seekBar>=0 ? this.state.seekBar : 0}
                        onValueChange={(seekBar) => this.controlTrack(seekBar)}
                        minimumTrackTintColor={"#fa4d09"}
                        thumbTintColor={"#FFFFFF"}
                        maximumValue={this.state.trackDuration>=0? this.state.trackDuration : 0}
                        minimumValue={0}
                    />
                    <Text ref = {'timetext'} style={{ alignSelf: "flex-end", width : 40, color: "white" }}>{this.state.remainingMinutes + this.state.showColonWithSeconds + this.state.remainingSeconds}</Text>
                </View>

                <View style={styleMediaScreen.multiMedia}>
                    <TouchableOpacity onPress={this.rewindTrack}
                        onLongPress={this.fastRewindTrack}>
                        <Image style={styleMediaScreen.iconNextPrev}
                            source={require("../../assests/prevTrack.png")}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.playPause}>
                        <Image style={styleMediaScreen.iconPlayPause}
                            source={this.state.trackActive ? require("../../assests/pause.png") : require("../../assests/play.png")}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.forwardTrack}
                        onLongPress={this.fastForwardTrack}>
                        <Image style={styleMediaScreen.iconNextPrev}
                            source={require("../../assests/nextTrack.png")}
                        />
                    </TouchableOpacity>

                </View>

                <View style={{ marginTop: 4, justifyContent: "flex-start", }}>
                    <View style={{ width: 120, marginLeft: 12 }}>
                        <Slider
                            value={this.state.volume}
                            onValueChange={(volume) => this.controlVolume(volume)}
                            minimumTrackTintColor={"#fa4d09"}
                            thumbTintColor={"#FFFFFF"}
                        />
                    </View>
                    <TouchableOpacity onPress={this.mute} style={{ alignSelf: 'flex-start' }}>
                        <Image style={[styleMediaScreen.iconNextPrev, { marginLeft: 12 }]}
                            source={this.state.mute ? require("../../assests/mute.png") : require("../../assests/volume.png")}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default TrackInfo


const styleMediaScreen = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: "#4c4c4c"
    },

    trackBox: {
        height: (metrics.deviceHeight * (35 / 100)),
        width: (metrics.deviceWidth * (85 / 100)),
        marginTop: 2,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fa4d09"
    },

    multiMedia: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        marginTop: 6
    },
    carousel: {
        height: (metrics.deviceHeight * (0.46)),
        width: metrics.deviceWidth - 2,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        marginTop: 0,
        marginLeft: 0,
        marginRight: 2
    },

    iconPlayPause: {
        resizeMode: "contain",
        width: 60,
        height: 60,
    },
    iconNextPrev: {
        resizeMode: "contain",
        width: 30,
        height: 30,
    },
    name: {
        fontSize: 14,
        color: "#FFFFFF",
        marginTop: 8,
        alignSelf: "flex-start",
        marginLeft: 28
    }

});