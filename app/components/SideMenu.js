import React, { Component } from 'react';
import { Image, ScrollView, Text, TouchableOpacity, View } from 'react-native'
//import Icon from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialIcons';

import MenuScreen from "../containers/MenuScreen";

import styles from './styles/doctor'

class SideMenu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView style={styles.modal}>
                <View style={styles.toolbar}>
                    <TouchableOpacity onPress={this.props.closeModal}>
                        <Icon name="close" size={30} color="#000000" />
                    </TouchableOpacity>
                    <View>
                        <MenuScreen />
                    </View>
                </View>
                <View>
                </View>
            </ScrollView>
        )
    }
}

export default SideMenu