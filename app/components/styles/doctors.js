import { Dimensions, Platform, StyleSheet } from 'react-native'
import Colors from './colors'

var { height, width } = Dimensions.get('window');
var cell_height = height * (28.7 / 100);
var cell_wid = width * (49.85 / 100);
var tid = width;
export default StyleSheet.create({
    cell: {
        flex: 1,
        justifyContent: 'center',
        //height: cell_height,
        width: cell_wid,
        backgroundColor: '#3b4045',
        //  marginLeft:0.5,
        marginTop: 0,
        marginBottom: 1,
        marginRight: 0.5
    },
    cell_m: {
        flex: 1,
         justifyContent: 'center',
        //height: cell_height,
        width: cell_wid,
        backgroundColor: '#3b4045',
        // marginLeft:0,
        marginTop: 0,
        marginBottom: 1,
        marginRight: 0
    },
    cellTitleList:{
        fontSize: 13,
        color: 'white',//Colors.text.highlight.dark,
        fontFamily: 'Arial',
        textAlignVertical: "center"
    },
    cellCountList:{
        fontSize: 13,
        marginRight:3,
        color: 'white',//Colors.text.highlight.dark,
        fontFamily: 'Arial',
        textAlignVertical: "center"
    },
    cellTitle: {            //names under pictures
        fontSize: 13,
        color: 'white',//Colors.text.highlight.dark,
        fontFamily: 'Arial',
        textAlignVertical: "center",
        paddingVertical:3,
        paddingHorizontal:7,
        // fontStyle: 'italic',

        //  backgroundColor:'#3b4045',//'#696969',
        //  height:50,
        //  width:cell_wid,
        //  marginTop:-16
    },
    container: {
        //alignSelf: 'stretch',
        alignItems: 'flex-start',
        backgroundColor: '#fa4d09',// Colors.background.dark,
        flex: 1,
        //marginTop: Platform.OS === 'ios' ? 20 : 0,
    },
    grid: {
        alignSelf: 'stretch',
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    modal: {
        backgroundColor: Colors.background.dark, //'#e978',//
        flex: 1,
        opacity: 0.9
    },
    thumbnail: {
        height: 151.5,
        width: cell_wid,
        margin: 0,
        padding: 0,
        resizeMode: 'cover'
    },
    menu1:
        {
            alignItems: 'flex-end',
            height: 35,
            width: 50,
            marginLeft: 0
        }
    ,
    logo: {
        height: 58,
        width: tid / 1.5,
        marginLeft: 10,
        //   alignItems:'center',
        //   marginTop:15,
        //   marginRight: 0
        //   resizeMode:"cover",
        //flex:1,
        //backgroundColor:"red"
    },
    toolbar: {          ///header is this
        /* alignItems: 'center',
         alignSelf: 'stretch',
         backgroundColor:'#3b4045',// Colors.background.primary,//'pink',
         flexDirection: 'row',
         //flex:1,
         height: 60,
          width:tid,
         justifyContent: 'flex-end'*/
        justifyContent: "space-between",
        height: 60,
        width: tid,
        flexDirection: "row",
        backgroundColor: '#3b4045'
        //backgroundColor:'yellow'
    },
    toolbarItem: {
        alignItems: 'center',
        alignSelf: 'stretch',
        justifyContent: 'center',
        //  backgroundColor:'yellow',
        flex: 1,
        flexDirection: 'row',
        marginRight: 10,
        height: 50,
        width: 40,
        padding: 5
    },
    toolbarItemText: {
        color: Colors.text.primary.light,
        fontSize: 16,
        fontFamily: 'FiraSans-Bold',
        marginRight: 10
    }
});