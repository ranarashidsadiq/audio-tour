import React, { Component } from 'react'
import { Image, Text, TouchableHighlight, View, Platform, TouchableOpacity } from 'react-native'

import { withNavigation } from 'react-navigation';

import styles from './styles/doctors'
import { metrics } from '../themes';

class AudioCelitem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            language: ""
        }
    }

    navigate = () => {
        this.props.navigation.navigate('Media', { language: this.props.language, doctor: this.props.doctor, track: this.props.language.isDutch ? this.props.doctor.audioDutch : this.props.doctor.audioBrabants });
    }

    render() {


        let doctor = this.props.doctor;
        var name = this.props.doctor.actor;
        name = name.toUpperCase();
        var odd = doctor.doctorNumber % 2;
        var image = <Image
            source={require('./images/play_live.png')}
            style={styles.thumbnail}
        />;

        switch (doctor.doctorNumber) {
            case 0:
                image = <Image
                    source={require('./images/intro.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 1:
                image = <Image
                    source={require('./images/profile_01.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 2:
                image = <Image
                    source={require('./images/profile_02.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 3:
                image = <Image
                    source={require('./images/profile_03.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 4:
                image = <Image
                    source={require('./images/profile_04.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 5:
                image = <Image
                    source={require('./images/profile_05.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 6:
                image = <Image
                    source={require('./images/profile_06.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 7:
                var image = <Image
                    source={require('./images/profile_07.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 8:
                var image = <Image
                    source={require('./images/profile_08.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 9:
                var image = <Image
                    source={require('./images/profile_09.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 10:
                var image = <Image
                    source={require('./images/profile_010.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 11:
                var image = <Image
                    source={require('./images/profile_011.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 12:
                var image = <Image
                    source={require('./images/profile_012.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 13:
                var image = <Image
                    source={require('./images/profile_013.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 14:
                var image = <Image
                    source={require('./images/profile_014.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 15:
                var image = <Image
                    source={require('./images/profile_015.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 16:
                var image = <Image
                    source={require('./images/profile_016.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 17:
                var image = <Image
                    source={require('./images/profile_017.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 18:
                var image = <Image
                    source={require('./images/profile_018.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 19:
                var image = <Image
                    source={require('./images/profile_019.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 20:
                var image = <Image
                    source={require('./images/profile_020.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 21:
                var image = <Image
                    source={require('./images/profile_021.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 22:
                var image = <Image
                    source={require('./images/profile_022.jpg')}
                    style={styles.thumbnail}
                />;
                break;
            case 23:
                var image = <Image
                    source={require('./images/profile_023.jpg')}
                    style={styles.thumbnail}
                />;
                break;

        }
        return (
            <View>
                {this.props.screenName === "floorListScreen" ? doctor.doctorNumber != 0 ?
                    <TouchableOpacity
                        onPress={this.navigate}>
                        <View>
                            <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 5, paddingLeft: 12 }}>
                                <Text style={[styles.cellCountList]}>{doctor.doctorNumber}.</Text>
                                <Text style={[styles.cellTitleList]}>{this.props.doctor.audioName}</Text>
                            </View>
                        </View>
                    </TouchableOpacity> :
                    null :
                    <View style={odd ? styles.cell_m : styles.cell}>
                        <TouchableHighlight
                            onPress={this.navigate}>
                            <View style={styles.cell}>

                                {image}
                                <Image
                                    style={{ position: "absolute", resizeMode: "contain", width: 60, height: 60, alignSelf: "center", marginTop: 40 }}
                                    source={require('../../assests/playLight.png')}
                                />
                                <View style={{ flexDirection: "row", alignItems: "center", alignContent: "center", }}>
                                    <Text style={[styles.cellTitle]}>{doctor.doctorNumber == 0 ? "" : doctor.doctorNumber > 0 && doctor.doctorNumber < 10 ? "0" + doctor.doctorNumber : doctor.doctorNumber}{" "}{name}</Text>
                                </View>

                            </View>
                        </TouchableHighlight>
                    </View>}
                {/* {this.props.screenName === "floorListScreen" ?
                    doctor.doctorNumber != 0 ?
                        <TouchableOpacity
                            onPress={this.navigate}>
                            <View>
                                <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 5, paddingLeft: 12 }}>
                                    <Text style={[styles.cellCountList]}>{doctor.doctorNumber}.</Text>
                                    <Text style={[styles.cellTitleList]}>{this.props.doctor.audioName}</Text>
                                </View>
                            </View>
                        </TouchableOpacity> :
                        null
                    :
                    <View style={odd ? styles.cell_m : styles.cell}>
                        <TouchableHighlight
                            onPress={this.navigate}>
                            <View style={styles.cell}>

                                {image}
                                <Image
                                    style={{ position: "absolute", resizeMode: "contain", width: 60, height: 60, alignSelf: "center", marginTop: 40 }}
                                    source={require('../../assests/playLight.png')}
                                />
                                <View style={{ flexDirection: "row", alignItems: "center", alignContent: "center", }}>
                                    <Text style={[styles.cellTitle]}>{doctor.doctorNumber == 0 ? "" : doctor.doctorNumber > 0 && doctor.doctorNumber < 10 ? "0" + doctor.doctorNumber : doctor.doctorNumber}{" "}{name}</Text>
                                </View>

                            </View>
                        </TouchableHighlight>
                    </View>} */}
            </View>

        )
    }
}

export default withNavigation(AudioCelitem); 
