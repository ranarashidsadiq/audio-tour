
import React, { Component } from 'react'
import { TouchableOpacity, Image, Text, StyleSheet, View, TextInput, ImageBackground } from 'react-native'

import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import Permissions from 'react-native-permissions'

import TopBar from '../components/TopBar';
import { withNavigation } from 'react-navigation';

import MenuScreen from "../containers/MenuScreen";
import { metrics, colors, constants, } from '../themes';
import ReactNativeZoomableView from '@dudigital/react-native-zoomable-view/src/ReactNativeZoomableView';
const doctorsData = require('../core-module/data/doctors.json');
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import ViewTransformer from 'react-native-view-transformer';


// import PinchZoomView from 'react-native-pinch-zoom-view';
// import {PinchView} from 'react-pinch-zoom-pan';



class FloorPlanItem extends Component {
    constructor(props) {
        super(props);
        // this.zoomTap = zoomTap.bind(this);
    }

    menuDrawer = () => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    };
    zoomTap = (id) => {

        this.props.navigation.navigate('Media', { language: this.props.language, doctor: doctorsData[id], track: this.props.language.isDutch ? doctorsData[id].audioDutch : doctorsData[id].audioBrabants });

    }


    render() {

        return (
            // <ReactNativeZoomableView
            //     maxZoom={2.0}
            //     minZoom={0.5}
            //     zoomStep={0.2}
            //     initialZoom={1.0}
            //     // bindToBorders={true}
            //     onZoomAfter={this.logOutZoomState}
            //     style={{
            //         padding: 10,
            //         alignItems:'center',
            //         backgroundColor: 'white',
            //     }}
            // >
            <ViewTransformer
                style={{
                    alignItems: 'center',
                    flex: 1
                }}
                maxScale={1.5} minScale={0.3} enableResistanc={false} maxOverScrollDistance={200}>
                <View style={{
                    height: 1000,
                    width: 1000,
                    backgroundColor: 'white',
                }}>


                    <Image style={{ width: 1000, height: 1000 }}
                        source={require('../../assests/Map.png')}
                        resizeMode="contain" />
                    <TouchableOpacity onPress={(data) => this.zoomTap(22)} style={[styleAgendaScreen.touchStyle, { top: 781, left: 150, }]}>
                        <Text style={styleAgendaScreen.doubleText}>22</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(21)} style={[styleAgendaScreen.touchStyle, { top: 660, left: 240, }]}>
                        <Text style={styleAgendaScreen.doubleText}>21</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(20)} style={[styleAgendaScreen.touchStyle, { top: 630, left: 450, }]}>
                        <Text style={styleAgendaScreen.doubleText}>20</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(23)} style={[styleAgendaScreen.touchStyle, { top: 708, left: 670, }]}>
                        <Text style={styleAgendaScreen.doubleText}>23</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(1)} style={[styleAgendaScreen.touchStyle, { top: 710, left: 715, }]}>
                        <Text style={styleAgendaScreen.doubleText}>1</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(7)} style={[styleAgendaScreen.touchStyle, { top: 336, left: 717, }]}>
                        <Text style={styleAgendaScreen.doubleText}>7</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(8)} style={[styleAgendaScreen.touchStyle, { top: 416, left: 682, }]}>
                        <Text style={styleAgendaScreen.doubleText}>8</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={(data) => this.zoomTap(9)} style={[styleAgendaScreen.touchStyle, { top: 435, left: 658, }]}>
                        <Text style={styleAgendaScreen.doubleText}>9</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(19)} style={[styleAgendaScreen.touchStyle, { top: 465, left: 658, }]}>
                        <Text style={styleAgendaScreen.doubleText}>19</Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={(data) => this.zoomTap(2)} style={[styleAgendaScreen.touchStyle, { top: 562, left: 680, }]}>
                        <Text style={styleAgendaScreen.doubleText}>2</Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={(data) => this.zoomTap(5)} style={[styleAgendaScreen.touchStyle, { top: 595, left: 680, }]}>
                        <Text style={styleAgendaScreen.doubleText}>5</Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={(data) => this.zoomTap(4)} style={[styleAgendaScreen.touchStyle, { top: 572, left: 630, }]}>
                        <Text style={styleAgendaScreen.doubleText}>4</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(3)} style={[styleAgendaScreen.touchStyle, { top: 555, left: 776, }]}>
                        <Text style={styleAgendaScreen.doubleText}>3</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(6)} style={[styleAgendaScreen.touchStyle, { top: 367, left: 844 }]}>
                        <Text style={styleAgendaScreen.doubleText}>6</Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={(data) => this.zoomTap(16)} style={[styleAgendaScreen.touchStyle, { top: 338, left: 550, }]}>
                        <Text style={styleAgendaScreen.doubleText}>16</Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={(data) => this.zoomTap(10)} style={[styleAgendaScreen.touchStyle, { top: 370, left: 545, }]}>
                        <Text style={styleAgendaScreen.doubleText}>10</Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={(data) => this.zoomTap(11)} style={[styleAgendaScreen.touchStyle, { top: 398, left: 532, }]}>
                        <Text style={styleAgendaScreen.doubleText}>11</Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={(data) => this.zoomTap(12)} style={[styleAgendaScreen.touchStyle, { top: 398, left: 469, }]}>
                        <Text style={styleAgendaScreen.doubleText}>12</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(15)} style={[styleAgendaScreen.touchStyle, { top: 339, left: 476, }]}>
                        <Text style={styleAgendaScreen.doubleText}>15</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(14)} style={[styleAgendaScreen.touchStyle, { top: 378, left: 375, }]}>
                        <Text style={styleAgendaScreen.doubleText}>14</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(13)} style={[styleAgendaScreen.touchStyle, { top: 408, left: 260, }]}>
                        <Text style={styleAgendaScreen.doubleText}>13</Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={(data) => this.zoomTap(17)} style={[styleAgendaScreen.touchStyle, { top: 208, left: 286, }]}>
                        <Text style={styleAgendaScreen.doubleText}>17</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={(data) => this.zoomTap(18)} style={[styleAgendaScreen.touchStyle, { top: 208, left: 150, }]}>
                        <Text style={styleAgendaScreen.doubleText}>18</Text>
                    </TouchableOpacity>

                </View>
            </ViewTransformer>
            // {/* </ReactNativeZoomableView> */}
        )
    }
}



const mapStateToProps = (state) => {
    let doctors = state.doctors.doctors;
    let language = state.language

    return {
        language: language,
    }
}
export default connect(mapStateToProps)(withNavigation(FloorPlanItem));


// export default withNavigation(FloorPlanItem);


const styleAgendaScreen = StyleSheet.create({
    doubleText: {
        color: "white",
        fontSize: 12,
        textAlign: "center"

    },
    touchStyle: {
        position: "absolute",
        backgroundColor: colors.orange,
        justifyContent: 'center',
        alignItems: "center",
        height: 25,
        width: 25,
        borderRadius: 20
    },
    topContainer: {
        flex: 1,
        alignItems: 'stretch',
        height: 290,
        width: metrics.deviceWidth,
        marginTop: 0,
    },
    topImage: {
        flexGrow: 1,
        height: null,
        width: null,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTextView: {
        height: 145,
        position: 'absolute',
        width: metrics.deviceWidth,
        marginTop: 0,
        justifyContent: 'center',
    },
    headerText: {
        color: colors.white,
        textAlign: 'center',
        fontSize: 30,
    },
    subHeaderText: {
        height: 145,
        position: 'absolute',
        color: colors.white,
        width: metrics.deviceWidth,
        marginTop: 145,
        textAlign: 'justify',
        lineHeight: 21,
        backgroundColor: colors.orangeTransparent,
        paddingHorizontal: 15,
        paddingVertical: 15,
    },
})


