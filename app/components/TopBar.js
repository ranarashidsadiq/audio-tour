import React, { Component, PropTypes } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';

import { colors, fonts, metrics } from "../themes";


import Icon from "react-native-vector-icons/MaterialIcons";

import { withNavigation } from 'react-navigation';

class TopBar extends Component {
    constructor(props) {
        super(props)
    }

    handlePressBack = () => {
        if (this.props.isFromIntro == true){
            this.props.goBack()
        }else{
            this.props.navigation.goBack()    
        }
        
    }


    render() {
        const { goBack, showBack, showLogo, showMenu, onClick, colorOrange } = this.props;
        return (
            <View style={styles.container}>
                {showBack ? <Icon
                    name="chevron-left"
                    size={metrics.icons.medium}
                    color={colors.white}
                    onPress={this.handlePressBack}
                /> : null}


                {showLogo ? <Image style={styles.icon}
                    source={showBack ? require('../../assests/logoGray.png') : require('../../assests/logoWhite.png')} /> : null}


                {showMenu ? <TouchableOpacity onPress={onClick}>
                    <Image style={styles.iconMenu}
                        source={colorOrange ? require('../../assests/menuOrange.png') : require('../../assests/menuWhite.png')} />
                </TouchableOpacity> : null}

            </View>
        );
    }

}

export default withNavigation(TopBar);

const styles = StyleSheet.create({
    container: {
        width: metrics.deviceWidth,
        flexDirection: 'row',
        height: 64,
        alignItems: 'center',
        backgroundColor: "black",
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    icon: {
        height: 46,
        width: metrics.images.logo,
        resizeMode: "contain",
        marginLeft: 12
    },
    iconMenu: {
        width: 48,
        marginRight: 12,
        resizeMode: "contain"
    }
});