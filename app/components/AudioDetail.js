import React, { Component } from 'react'
import { Image, ScrollView, Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

import styles from './styles/doctor'

class AudioDetail extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let doctor = this.props.doctor;
        return (
            <ScrollView style={styles.modal}>
                <View style={styles.toolbar}>
                    <TouchableOpacity onPress={this.props.closeModal}>
                        <Icon name="close" size={30} color="#FFF" />
                    </TouchableOpacity>
                </View>
                <View>
                </View>
            </ScrollView>
        )
    }
}

export default AudioDetail