import React, { Component, PropTypes } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';

import { colors, fonts, metrics } from "../themes";
import email from 'react-native-email'
import { withNavigation } from 'react-navigation';

class WorkshopItem extends Component {
    constructor(props) {
        super(props)
    }
    sendMail = () => {
        const to = [this.props.Item.email] // string or array of email addresses
        email(to, {
            // Optional additional arguments
            subject: this.props.Item.title,
            body: 'Heeft u intereresse in onze workshop? Vul dan het onderstaande formulier in:<br>' +
                'Naam			:<br>' +
                'Telefoonnummer	:<br>' +
                'E-mail			:<br>' +
                'Evt. opmerkingen	:<br>'
        }).catch(console.error)
    }
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    disabled={this.props.comleteDescription}
                    onPress={() => {
                        this.props.navigation.navigate('Agenda', {
                            screenName: "WORKSHOP",
                            Item: this.props.Item
                        });
                    }}>
                    <Text style={styles.headerText}>{this.props.Item.title}</Text>
                    <Text style={styles.detailText}>{this.props.comleteDescription ? this.props.Item.text : this.props.Item.text.substring(0, 500) + " [...]"}</Text>
                    <Text style={styles.durationText}>Duur: {this.props.Item.duur}</Text>
                    <Text style={styles.durationText}>Kosten: {this.props.Item.kosten}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.sendMail}>
                    <Text style={styles.reservationButton}>RESERVEER DIRECT</Text>
                </TouchableOpacity>
            </View>
        );
    }

}

export default withNavigation(WorkshopItem);

const styles = StyleSheet.create({
    container: {
        width: metrics.deviceWidth,
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingVertical: 15,
       paddingBottom:1,
    },
    headerText: {
        color: colors.orange,
        paddingBottom:3,
        fontWeight:"700",
        fontSize:15,
        fontFamily:"FontAwesome",
    },
    durationText: {
        color: colors.orange,
        paddingTop:3,
    },
    detailText: {
        color: colors.blue1,
        fontFamily:"FontAwesome",
        paddingBottom:4,
        lineHeight:18
    },
    reservationButton: {
        width:165,
        textAlign: 'center',
        marginTop: 10,
        color: colors.white,
        backgroundColor: colors.blue1,
        padding:10,
        fontFamily:"FontAwesome",

    }
});