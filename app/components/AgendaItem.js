import React, { Component, PropTypes } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';

import { colors, fonts, metrics } from "../themes";
import Moment from 'moment';
import { withNavigation } from 'react-navigation';
var nlLocale = require('moment/locale/nl'); 
Moment.locale("nl");
class AgendaItem extends Component {
    constructor(props) {
        super(props);
        console.log(this.props);
        debugger;
    }


    render() {
       console.log(this.props.Item['meta-fields']['event-date'][0]);
       console.log(nlLocale);
       debugger;
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    disabled={this.props.comleteDescription}
                    onPress={() => {
                        this.props.navigation.navigate('Agenda', {
                            screenName: "AGENDA",
                            Item: this.props.Item
                        });
                    }}>
                    <Text style={styles.headerText}>{this.props.Item.title.rendered.toUpperCase()}</Text>

                    <Text style={styles.dateText}>Datum: { Moment.unix(this.props.Item['meta-fields']['event-date'][0]).format("DD MMMM YYYY").toUpperCase()}</Text>
                    <Text style={styles.dateText}>Tijd: {this.props.Item['meta-fields']['event-time']}</Text>
                    <Text style={styles.detailText}>{this.props.comleteDescription ? this.props.Item.content.rendered : this.props.Item.content.rendered.substring(0, 300) + " [...]"}</Text>
                </TouchableOpacity>
            </View>
        );
    }

}

export default withNavigation(AgendaItem);

const styles = StyleSheet.create({
    container: {
        width: metrics.deviceWidth,
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingVertical: 15,
    },
    headerText: {
        color: colors.orange,
        textDecorationLine: 'underline',
        paddingBottom:3,
        fontFamily:"FontAwesome",
        fontSize:18


    },
    dateText: {
        color: colors.grayDark,
        paddingTop:3
    },
    detailText: {
        color: colors.blue1,
        fontFamily:"FontAwesome",
        paddingTop:3,
        lineHeight:18
    }
});