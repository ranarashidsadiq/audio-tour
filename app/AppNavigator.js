import { Platform } from "react-native";

import { StackNavigator } from "react-navigation";

import IntroScreen from './containers/IntroScreen'
import AudioList from './containers/AudioList'
import MediaScreen from './containers/MediaScreen'
import MenuScreen from './containers/MenuScreen'
import ContactScreen from './containers/ContactScreen'
import InfoScreen from './containers/InfoScreen'
import AgendaListScreen from "./containers/AgendaListScreen";
import AgendaScreen from "./containers/AgendaScreen";
import ListFloorPlanScreen from "./containers/ListFloorPlanScreen";
import FloorPlanScreen from "./containers/FloorPlanScreen";

const Routes = {
    Intro: {
        name: "Intro",
        navigationOptions: {
            header: null
        },
        screen: IntroScreen,
        mode: 'modal'
    },
    Audio: {
        name: "Audio",
        navigationOptions: {
            title: "Audio",
            header: null,
            headerLeft: null
        },
        screen: AudioList
    },
    Media: {
        name: "Media",
        navigationOptions: {
            title: "Media",
            header: null,
            headerLeft: null
        },
        screen: MediaScreen
    },
    AgendaList: {
        name: "Agenda",
        navigationOptions: {
            title: "Agenda",
            header: null,
            headerLeft: null
        },
        screen: AgendaListScreen
    },
    Agenda: {
        name: "Agenda",
        navigationOptions: {
            title: "Agenda",
            header: null,
            headerLeft: null
        },
        screen: AgendaScreen
    },
    ListFloorPlan: {
        name: "FloorPlan",
        navigationOptions: {
            title: "FloorPlan",
            header: null,
            headerLeft: null
        },
        screen: ListFloorPlanScreen
    },
    FloorPlan: {
        name: "FloorPlan",
        navigationOptions: {
            title: "FloorPlan",
            header: null,
            headerLeft: null
        },
        screen: FloorPlanScreen
    },
    Menu: {
        name: "Menu",
        navigationOptions: {
            header: null,
            headerLeft: null
        },
        screen: MenuScreen
    },
    Contact: {
        name: "Contact",
        navigationOptions: {
            title: "Contact",
            header: null,
            headerLeft: null
        },
        screen: ContactScreen
    },
    Info: {
        name: "Info",
        navigationOptions: {
            title: "Info",
            header: null,
            headerLeft: null
        },
        screen: InfoScreen
    }
};

export default (AppNavigator = StackNavigator(
    {
        ...Routes
    },
    {
        headerMode: "screen",
        /*
         * Use modal on iOS because the card mode comes from the right,
         * which conflicts with the drawer example gesture
         */
        mode: "card",
        navigationOptions: {
            headerTitleStyle: {
                color: "#FFFFFF",
                alignSelf: "center",
                fontWeight: 100
            },
            headerStyle: {
                borderBottomWidth: 1,
                borderBottomColor: "#000000",
                elevation: 0
            }
        }
    }
));