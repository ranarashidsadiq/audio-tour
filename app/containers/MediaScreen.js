import React, { Component } from 'react';
import { Dimensions, Modal, View, StyleSheet, Text, Image, TouchableOpacity, Platform } from 'react-native';

import KeepAwake from 'react-native-keep-awake';

// import Sound from "react-native-sound";

import MenuScreen from "../containers/MenuScreen";

import Slider from "react-native-slider";

// import Carousel, { Pagination } from 'react-native-snap-carousel';

import TopBar from "../components/TopBar"
import TrackInfo from "../components/TrackInfo"
import { colors, metrics } from '../themes';
// import SideSwipe from 'react-native-sideswipe';
import Carousel from "react-native-carousel-control";
// const doctorsData = require('../core-module/data/doctors.json');

var audioTime;
var whoosh;
let deviceWidth = Dimensions.get('window').width;

class MediaScreen extends Component {



    constructor(props) {
        super(props);

        this.state = {
            showMenu: false,
            // trackActive: false,
            // actorName: "",
            // volume: 0.5,
            // seekBar: 0.00,
            // mute: false,
            // loadCarousel: true,
            // trackDuration: 0.0,
            // totalMinutes: 0,
            // totalSeconds: 0,
            // remainingMinutes: 0,
            // remainingSeconds: 0,
            // showColonWithZero: ":",
            // showColonWithSeconds: ":0",
            // img: "",
            // trackNumber: -1,
            // loadFirst: false,
            // entries: doctorsData
        }
    }

    componentWillMount() {
        // KeepAwake.activate();
        // switch (this.props.navigation.state.params.doctor.doctorNumber) {
        //     case 0:
        //         this.setState({
        //             img: require("../components/images/intro.jpg")
        //         });
        //         break;
        //     case 1:
        //         this.setState({
        //             img: require("../components/images/profile_01.jpg")
        //         });
        //         break;
        //     case 2:
        //         this.setState({
        //             img: require("../components/images/profile_02.jpg")
        //         });
        //         break;
        //     case 3:
        //         this.setState({
        //             img: require("../components/images/profile_03.jpg")
        //         });
        //         break;
        //     case 4:
        //         this.setState({
        //             img: require("../components/images/profile_04.jpg")
        //         });
        //         break;
        //     case 5:
        //         this.setState({
        //             img: require("../components/images/profile_05.jpg")
        //         });
        //         break;
        //     case 6:
        //         this.setState({
        //             img: require("../components/images/profile_06.jpg")
        //         });
        //         break;
        //     case 7:
        //         this.setState({
        //             img: require("../components/images/profile_07.jpg")
        //         });
        //         break;
        //     case 8:
        //         this.setState({
        //             img: require("../components/images/profile_08.jpg")
        //         });
        //         break;
        //     case 9:
        //         this.setState({
        //             img: require("../components/images/profile_09.jpg")
        //         });
        //         break;
        //     case 10:
        //         this.setState({
        //             img: require("../components/images/profile_010.jpg")
        //         });
        //         break;
        //     case 11:
        //         this.setState({
        //             img: require("../components/images/profile_011.jpg")
        //         });
        //         break;
        //     case 12:
        //         this.setState({
        //             img: require("../components/images/profile_012.jpg")
        //         });
        //         break;
        //     case 13:
        //         this.setState({
        //             img: require("../components/images/profile_013.jpg")
        //         });
        //         break;
        //     case 14:
        //         this.setState({
        //             img: require("../components/images/profile_014.jpg")
        //         });
        //         break;
        //     case 15:
        //         this.setState({
        //             img: require("../components/images/profile_015.jpg")
        //         });
        //         break;
        //     case 16:
        //         this.setState({
        //             img: require("../components/images/profile_016.jpg")
        //         });
        //         break;
        //     case 17:
        //         this.setState({
        //             img: require("../components/images/profile_017.jpg")
        //         });
        //         break;
        //     case 18:
        //         this.setState({
        //             img: require("../components/images/profile_018.jpg")
        //         });
        //         break;
        //     case 19:
        //         this.setState({
        //             img: require("../components/images/profile_019.jpg")
        //         });
        //         break;
        //     case 20:
        //         this.setState({
        //             img: require("../components/images/profile_020.jpg")
        //         });
        //         break;
        //     case 21:
        //         this.setState({
        //             img: require("../components/images/profile_021.jpg")
        //         });
        //         break;
        //     case 22:
        //         this.setState({
        //             img: require("../components/images/profile_022.jpg")
        //         });
        //         break;
        //     case 23:
        //         this.setState({
        //             img: require("../components/images/profile_023.jpg")
        //         });
        //         break;
        // }
        // let name = this.props.navigation.state.params.doctor.actor
        // name = name.toUpperCase();
        // this.setState({
        //     actorName: name
        // });
    }



    menuDrawer = () => {
        this.setState({
            showMenu: !this.state.showMenu,
        })
    };

    // makeTrack = (track) => {
    //     if (whoosh != null) {
    //         whoosh.stop();
    //         if (audioTime != null) {
    //             clearInterval(audioTime);
    //         }
    //     }
    //     whoosh = new Sound(track, Sound.MAIN_BUNDLE, (error) => {
    //         whoosh.setCategory('Playback')
    //         if (error) {
    //             console.log('failed to load the sound', error);
    //             return;
    //         }
    //         // loaded successfully
    //         setTimeout(() => {
    //             whoosh.play();
    //         }, 1000);
    //         if (this.state.mute == true){
    //             this.setState({
    //                 volume: 0
    //             });
    //         }
    //         whoosh.setVolume(this.state.volume);
    //         this.setState({
    //             trackDuration: whoosh.getDuration()
    //         });

    //         var mind = whoosh.getDuration() % (60 * 60);
    //         var minutes = Math.floor(mind / 60);

    //         var secd = mind % 60;
    //         var seconds = Math.ceil(secd);

    //         if (seconds < 10) {
    //             this.setState({
    //                 showColonWithZero: ":0"
    //             });
    //         } else {
    //             this.setState({
    //                 showColonWithZero: ":"
    //             });
    //         }

    //         this.setState({
    //             totalMinutes: minutes,
    //             totalSeconds: seconds
    //         })

    //         audioTime = setInterval(this.secToMin, 500);

    //         //alert('duration in seconds: ' + whoosh.getNumberOfChannels());
    //     });
    // };


    componentDidMount() {
        // this.setState({
        //     trackActive: true
        // });
        // this.setState({ trackNumber: 0 })
        // setTimeout(() => {
        //     // this.setState({loadFirst : true})
        //     this.refs.carousel.moveNext();
        // }, 400);
        // var doctor = doctorsData[0]

        // var track = this.props.navigation.state.params.language.isDutch ? doctor.audioDutch : doctor.audioBrabants
        // this.makeTrack(track)
        // this.makeTrack(this.props.navigation.state.params.track)

    }

    componentWillUnmount() {
        // whoosh.stop();
        // KeepAwake.deactivate();
        // clearInterval(audioTime);
    }

    // playPause = () => {
    //     this.state.trackActive ? whoosh.pause() : whoosh.play();
    //     this.setState({
    //         trackActive: !this.state.trackActive
    //     });
    // }

    // fastForwardTrack = () => {
    //     let forward = this.state.seekBar
    //     forward = forward + 8;
    //     whoosh.setCurrentTime(forward);
    // }

    // forwardTrack = () => {
    //     let forward = this.state.seekBar
    //     forward = forward + 10;
    //     whoosh.setCurrentTime(forward);
    // }

    // fastRewindTrack = () => {
    //     let rewind = this.state.seekBar
    //     rewind = rewind - 8;
    //     whoosh.setCurrentTime(rewind);
    // }

    // rewindTrack = () => {
    //     let rewind = this.state.seekBar
    //     rewind = rewind - 10;
    //     whoosh.setCurrentTime(rewind);
    // }

    // secToMin = () => {

    //     whoosh.getCurrentTime((seconds) => {
    //         var mind = seconds % (60 * 60);
    //         var trackMinutes = Math.floor(mind / 60);

    //         var secd = mind % 60;
    //         var trackSeconds = Math.ceil(secd);

    //         if (trackSeconds < 10) {
    //             this.setState({
    //                 showColonWithSeconds: ":0"
    //             });
    //         } else {
    //             this.setState({
    //                 showColonWithSeconds: ":"
    //             });
    //         }

    //         this.setState({
    //             seekBar: seconds,
    //             remainingMinutes: trackMinutes,
    //             remainingSeconds: trackSeconds
    //         });

    //     });

    //     if (this.state.remainingMinutes == this.state.totalMinutes && this.state.remainingSeconds == this.state.totalSeconds) {
    //         this.setState({
    //             trackActive: false,
    //         });
    //         whoosh.setCurrentTime(0);
    //         whoosh.stop();
    //     }

    // }

    // controlVolume = (volume) => {
    //     whoosh.setVolume(volume);
    //     if (volume == 0) {
    //         this.setState({
    //             mute: true
    //         });
    //     } else {
    //         this.setState({
    //             mute: false
    //         });
    //     }
    //     this.setState({
    //         volume: volume
    //     });

    // }

    // controlTrack = (seekBar) => {
    //     this.setState({
    //         seekBar: seekBar
    //     });
    //     whoosh.setCurrentTime(seekBar);
    // }


    // mute = () => {
    //     if (!this.state.mute) {
    //         this.setState({
    //             mute: true
    //         });
    //         whoosh.setVolume(0);
    //     } else {
    //         this.setState({
    //             mute: false
    //         });
    //         whoosh.setVolume(this.state.volume);
    //     }

    // }

    onPageChanged = (sliderIndex) => {

        this.refs.TrackInfo.loadTrack(sliderIndex)
        // var doctor = doctorsData[sliderIndex]

        // var track = this.props.navigation.state.params.language.isDutch ? doctor.audioDutch : doctor.audioBrabants
        // this.makeTrack(track)
        // this.setState({ trackNumber: sliderIndex,loadFirst : true, trackActive: true, actorName: doctor.actor.toUpperCase() })

    }

    // _renderItem = ({ item, index }) => {
    //     // alert(index)
    //     // var currentItem = this.refs.carousel.currentScrollPosition
    //     switch (item.doctorNumber) {
    //         case 0:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/intro.jpg")} />
    //             );
    //         case 1:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_01.jpg")} />
    //             );

    //         case 2:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_02.jpg")} />
    //             );

    //         case 3:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_03.jpg")} />
    //             );

    //         case 4:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_04.jpg")} />
    //             );
    //         case 5:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_05.jpg")} />
    //             );
    //         case 6:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_06.jpg")} />
    //             );
    //         case 7:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_07.jpg")} />
    //             );
    //         case 8:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_08.jpg")} />
    //             );
    //         case 9:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_09.jpg")} />
    //             );
    //         case 10:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_010.jpg")} />
    //             );
    //         case 11:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_011.jpg")} />
    //             );
    //         case 12:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_012.jpg")} />
    //             );
    //         case 13:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_013.jpg")} />
    //             );
    //         case 14:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_014.jpg")} />
    //             );
    //         case 15:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_015.jpg")} />
    //             );
    //         case 16:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_016.jpg")} />
    //             );
    //         case 17:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_017.jpg")} />
    //             );
    //         case 18:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_018.jpg")} />
    //             );
    //         case 19:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_019.jpg")} />
    //             );
    //         case 20:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_020.jpg")} />
    //             );
    //         case 21:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_021.jpg")} />
    //             );
    //         case 22:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_022.jpg")} />
    //             );
    //         case 23:
    //             return (
    //                 <Image style={styleMediaScreen.trackBox}
    //                     source={require("../components/images/profile_023.jpg")} />
    //             );
    //     }

    // }

    render() {
        const { navigation } = this.props;
        var trackView = null;


        //                    <SideSwipe
        //         ref={'carousel'}
        // index={this.props.navigation.state.params.doctor.doctorNumber}
        // itemWidth={deviceWidth * (85 / 100)}
        // style = {{deviceWidth}}
        // data={doctorsData}
        // threshold={150}
        // contentOffset={(deviceWidth - (deviceWidth * (85 / 100)))/2}
        // onIndexChange={this._snapItemCarousel}
        // renderItem={this._renderItem}
        // />;

        // alert('test')


        //     <Carousel
        //     layout={'default'}
        //     ref={'carousel'}
        //     data={this.state.entries}
        //     renderItem={this._renderItem}
        //     onSnapToItem={this._snapItemCarousel}
        //     // sliderWidth={(deviceWidth - (deviceWidth * 0.04))}
        //                             sliderWidth={Platform.OS === 'ios' ? (deviceWidth - (deviceWidth * 0.04)) : (deviceWidth - (deviceWidth * 0.01))}

        //     // itemWidth={262}
        //     // sliderWidth={(deviceWidth - (deviceWidth*0.0781))}
        //     itemWidth={(deviceWidth * 0.81875)}
        //     firstItem={this.state.loadFirst ? this.props.navigation.state.params.doctor.doctorNumber : 0}

        // />;

        // alert(deviceWidth);
        // if (this.state.trackNumber != -1) {
        trackView = <View style={styleMediaScreen.container}>
            <View >
                <TopBar showBack={true} showLogo={true} showMenu={true} onClick={this.menuDrawer} />
            </View>

            {/* <View style={{ alignItems: "center" }}>
                <Image style={styleMediaScreen.trackBox}
                    source={this.state.img} />
                <Text style={styleMediaScreen.name}>
                    {this.state.actorName}
                </Text>
            </View> */}
            <View style={styleMediaScreen.carousel}>
                <Carousel sneak={Platform.OS === 'ios' ? 30 : 0} onPageChange={this.onPageChanged} swipeThreshold={0.2} currentPage={this.props.navigation.state.params.doctor.doctorNumber}>
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/intro.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_01.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_02.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_03.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_04.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_05.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_06.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_07.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_08.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_09.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_010.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_011.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_012.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_013.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_014.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_015.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_016.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_017.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_018.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_019.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_020.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_021.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_022.jpg")} />
                    <Image style={styleMediaScreen.trackBox}
                        source={require("../components/images/profile_023.jpg")} />

                </Carousel>
            </View>
            <TrackInfo
                ref={"TrackInfo"}
                audioTime={audioTime}
                currentindex={this.props.navigation.state.params.doctor.doctorNumber}
                whoosh={whoosh}
                language={this.props.navigation.state.params.language}
                doctor={this.props.navigation.state.params.doctor}
                track={this.props.navigation.state.params.language.isDutch ? this.props.navigation.state.params.doctor.audioDutch : this.props.navigation.state.params.doctor.audioBrabants}
            ></TrackInfo>
            <Modal
                animationType={"slide"}
                transparent={true}
                visible={this.state.showMenu}
                onRequestClose={this.props.showDoctorslist}
            >
                <MenuScreen menuDrawer={this.menuDrawer} colorOrange={true} />
            </Modal>
        </View>
        return (
            trackView
        )
    }
}

export default MediaScreen


const styleMediaScreen = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: "#4c4c4c"
    },

    trackBox: {
        height: (metrics.deviceHeight * (35 / 100)),
        width: Platform.OS === 'ios' ? metrics.deviceWidth * (79 / 100) : metrics.deviceWidth * (89 / 100),
        marginTop: 2,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fa4d09",
    },

    multiMedia: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        marginTop: 6
    },
    carousel: {
        height: (metrics.deviceHeight * (0.40)),
        width: Platform.OS === 'ios' ? metrics.deviceWidth: metrics.deviceWidth * (89 / 100),
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        marginTop: 0,
        marginLeft: Platform.OS === 'ios' ? 0: (metrics.deviceWidth * (11 / 100))/2
    },
    // carousel: {
    //     alignItems: "center",
    //     marginTop: 0,
    // },

    iconPlayPause: {
        resizeMode: "contain",
        width: 60,
        height: 60,
    },
    iconNextPrev: {
        resizeMode: "contain",
        width: 30,
        height: 30,
    },
    name: {
        fontSize: 14,
        color: "#FFFFFF",
        marginTop: 8,
        alignSelf: "flex-start",
        marginLeft: 28
    }

});