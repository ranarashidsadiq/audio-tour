import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { ListView } from 'react-native'
import { orderByName, orderByNumber, selectDoctor, showDoctorslist, setLanguage } from '../core-module/actions'
import HomeComponent from '../components/Home'

const doctorsData = require('../core-module/data/doctors.json');

const ds = new ListView.DataSource({
    rowHasChanged: (r1, r2) => r1 !== r2
});

const mapStateToProps = (state) => {
    let doctors = state.doctors.doctors;
    let language = state.language

    return {
        language: language,
        doctors: doctorsData,
        doctorsDataSource: ds.cloneWithRows(doctors),
        showDoctor: state.doctors.showDoctor,
        doctor: state.doctors.doctor
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        orderByName,
        orderByNumber,
        selectDoctor,
        showDoctorslist,
        setLanguage
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);