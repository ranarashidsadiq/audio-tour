import React, { Component } from 'react'
import { Text, Modal, TouchableOpacity, View, Image, StyleSheet, AsyncStorage, Platform } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { EUROPE, ENGLISH, GERMANY, FRANCE } from "../core-module/actions/type"

import { setLanguage } from "../core-module/actions"

import styles from '../components/styles/doctors';
import { metrics } from '../themes';
import InfoScreen from "../containers/InfoScreen";
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

class IntroScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showLanguage: false,
            showMenu: false,
            isFromIntro: true
        }
    }
    openInfo = () => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    };
    changeLanguage = (language) => {
        this.hideLanguage();
        if (language == "BRABANT") {
            this.props.language.isDutch = false
        } else {
            this.props.language.isDutch = true
        }
        this.props.setLanguage(language);
        this.props.navigation.navigate('Audio');
    }


    showLanguage = async () => {
        const verifed = await AsyncStorage.getItem("@showLanguage:key");
        if (verifed !== null) {
            this.props.navigation.navigate('Audio');
            this.setState({
                showLanguage: false
            });
        } else {
            this.setState({
                showLanguage: true
            });
        }
    };

    hideLanguage = async () => {
        await AsyncStorage.setItem("@showLanguage:key", "hideLanguage");
        this.setState({
            showLanguage: false
        });

    }
    onSwipeUp(gestureState) {
        this.openInfo()
      }

    componentWillMount() {
        this.showLanguage();
    }

    swipe = () => {
        this.props.navigation.navigate('Info', { fromSwipe: true });
    }


    render() {
        const config = {
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80
          };

        return (
            <View style={styleIntroScreen.container}>

                <Image source={require('../../assests/bg.png')}
                    style={Platform.OS ==='ios' ? styleIntroScreen.backgroundImage_ios:styleIntroScreen.backgroundImage }>

                </Image>
                <Image style={styleIntroScreen.logo}
                    source={require("../../assests/logo.png")}
                />

                {this.state.showLanguage ? <View>
                    <Text style={styleIntroScreen.chooseLang}>
                        CHOOSE YOUR LANGUAGE
                    </Text>
                    <View style={styleIntroScreen.languages}>
                        <TouchableOpacity onPress={() => this.changeLanguage("DUTCH")}><Image style={styleIntroScreen.imageFlag}
                            source={require("../../assests/europe.png")} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.changeLanguage("BRABANT")}><Image style={styleIntroScreen.imageFlag}
                            source={require("../../assests/brabant.png")} />
                        </TouchableOpacity>
                    </View>
                </View> : null}

                <GestureRecognizer
                        onSwipeUp={(state) => this.onSwipeUp(state)}
                        config={config}
                        style={styleIntroScreen.info}
                        >
                        <Text style={styleIntroScreen.infoText}>SWIPE FOR INFO</Text>
                        <Image style={styleIntroScreen.infoImage} source={require("../../assests/arrow.png")} />
                    </GestureRecognizer>
                {/* <TouchableOpacity style={styleIntroScreen.info} onPressOut={this.openInfo} > */}
                    
                        
                    
                {/* </TouchableOpacity> */}
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.showMenu}
                    onRequestClose={this.props.showDoctorslist}
                >
                    <InfoScreen isFromIntro={true} menuBack={this.openInfo} colorOrange={true} />
                </Modal>
            </View>
        )
    }
}

const mapSateToProps = state => ({
    language: state.language,
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            setLanguage
        },
        dispatch
    );
};
export default connect(mapSateToProps, mapDispatchToProps)(IntroScreen);


const styleIntroScreen = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: "#4c4c4c",
        alignItems: "center"
    },

    backgroundImage: {
        justifyContent: 'center',
        alignItems: 'center',
        resizeMode: "contain",
        position: "absolute",
        backgroundColor: "#fa4d09",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    backgroundImage_ios: {
        justifyContent: 'center',
        alignItems: 'center',
        resizeMode: "contain",
        position: "absolute",
        backgroundColor: "#fa4d09",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        height: metrics.deviceHeight,
        width: metrics.deviceWidth
    },

    logo: {
        marginTop: 60,
        resizeMode: "contain"

    },

    chooseLang: {
        textAlign: "center",
        fontSize: 12,
        marginTop: 200,
        width: 320,
        color: "#FFFFFF",
        backgroundColor: "transparent"
    },

    languages: {
        flexDirection: "row",
        justifyContent: "space-around",
        paddingHorizontal: 80
    },

    info: {
        alignSelf: "center",
        alignItems: "center",
        position: "absolute",
        bottom: 18,
        left: 0,
        right: 0
    },

    infoText: {
        color: "#FFFFFF",
        fontSize: 12

    },

    imageFlag: {
        width: 60,
        height: 60,
        resizeMode: 'contain'
    },

    infoImage: {
        resizeMode: "contain",
        width: 25,
        height: 25,
    }


})