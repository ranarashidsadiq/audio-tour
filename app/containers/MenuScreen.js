import React, { Component } from 'react'
import {
    ScrollView, ListView, Modal, Text, TouchableHighlight, TouchableOpacity, View, Image, StyleSheet, AsyncStorage, Platform
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import { setLanguage } from "../core-module/actions"

import { withNavigation } from 'react-navigation';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import TopBar from "../components/TopBar";

import styles from '../components/styles/doctors';
import { metrics, colors } from '../themes';

class MenuScreen extends Component {
    constructor(props) {
        super(props);


        this.state = {
            language: ""
        }
    }

    changeLanguage = (language) => {
        this.hideLanguage();
        if (language == "BRABANT") {
            this.props.language.isDutch = false
        } else {
            this.props.language.isDutch = true
        }
        this.props.setLanguage(language);
        this.props.menuDrawer();
        this.props.navigation.navigate('Audio');
    }

    hideLanguage = async () => {
        await AsyncStorage.setItem("@showLanguage:key", "hideLanguage");
    }

    info = () => {
        this.props.menuDrawer();
        this.props.navigation.navigate('Info');
    }

    contact = () => {
        this.props.menuDrawer();
        this.props.navigation.navigate('Contact');
    }

    workshop = () => {
        this.props.menuDrawer();
        if (this.props.agendaScreen == true){
            this.props.navigation.goBack();  
        }
        this.props.navigation.navigate('AgendaList', {
            screenName: "WORKSHOP",
        });
    }

    agenda = () => {
        this.props.menuDrawer();
        if (this.props.agendaScreen == true){
            this.props.navigation.goBack();  
        }
        this.props.navigation.navigate('AgendaList', {
            screenName: "AGENDA",
        });

    }

    floorMap = () => {
        this.props.menuDrawer();
        this.props.navigation.navigate('FloorPlan');
    }

    render() {

        return (
            <View style={styleMenuScreen.container}>
                <View >
                    <TopBar showBack={false} showLogo={true} showMenu={true} onClick={this.props.menuDrawer} colorOrange={this.props.colorOrange} />
                </View>
                <ScrollView>
                    <View style={styleMenuScreen.contactInfo}>
                        <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
                            <TouchableHighlight underlayColor={colors.orange} activeOpacity={0.2} onPress={this.workshop} style={styleMenuScreen.viewCenter}>
                                <View style={[styleMenuScreen.contactInfoItem, styleMenuScreen.borderRight]}>
                                    <Image style={styleMenuScreen.icon}
                                        source={require("../../assests/icon-1.png")} />
                                    <Text style={[styleMenuScreen.text, { color: "#000000" }]}>WORKSHOP</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={colors.orange} activeOpacity={0.2} onPress={this.agenda} style={styleMenuScreen.viewCenter}>
                                <View style={styleMenuScreen.contactInfoItem}>
                                    <Image style={styleMenuScreen.icon}
                                        source={require("../../assests/icon-2.png")} />
                                    <Text style={[styleMenuScreen.text, { color: "#000000" }]}>AGENDA</Text>
                                </View>
                            </TouchableHighlight>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
                            <TouchableHighlight underlayColor={colors.orange} activeOpacity={0.2} onPress={this.contact} style={styleMenuScreen.viewCenter}>
                                <View style={[styleMenuScreen.contactInfoItem, styleMenuScreen.borderRight]}>
                                    <Image style={styleMenuScreen.icon}
                                        source={require("../../assests/icon-3.png")} />
                                    <Text style={[styleMenuScreen.text, { color: "#000000" }]}>CONTACT</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={colors.orange} activeOpacity={0.2} onPress={this.info} style={styleMenuScreen.viewCenter}>
                                <View style={styleMenuScreen.contactInfoItem}>
                                    <Image style={styleMenuScreen.icon}
                                        source={require("../../assests/icon-4.png")} />
                                    <Text style={[styleMenuScreen.text, { color: "#000000" }]}>INFO</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                        <View style={{ flexDirection: "row", justifyContent: "flex-start" }}>
                            <TouchableHighlight underlayColor={colors.orange} activeOpacity={0.2} onPress={this.floorMap} style={styleMenuScreen.viewCenter}>
                                <View style={[styleMenuScreen.contactInfoItem, styleMenuScreen.borderRight]}>

                                    <Image style={styleMenuScreen.icon}
                                        source={require("../../assests/icon-5.png")} />
                                    <Text style={[styleMenuScreen.text, { color: "#000000" }]}>PLATTEGROND</Text>
                                </View>
                            </TouchableHighlight>
                            <View style={[styleMenuScreen.contactInfoItem, {}]}>

                            </View>
                        </View>


                    </View>


                    <View style={styleMenuScreen.languageContainer}>
                        <Text style={styleMenuScreen.chooseLang}>
                            CHOOSE YOUR LANGUAGE
                    </Text>
                        <View style={styleMenuScreen.languages}>
                            <TouchableOpacity onPress={() => this.changeLanguage("DUTCH")}><Image style={styleMenuScreen.imageFlag}
                                source={require("../../assests/europe.png")} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.changeLanguage("BRABANT")}><Image style={styleMenuScreen.imageFlag}
                                source={require("../../assests/brabant.png")} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </ScrollView>
            </View>
        )
    }
}

const mapSateToProps = state => ({
    language: state.language,
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            setLanguage
        },
        dispatch
    );
};

export default connect(mapSateToProps, mapDispatchToProps)(withNavigation(MenuScreen));

//export default withNavigation(MenuScreen);


const styleMenuScreen = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        //alignItems: "center"
    },

    icon: {
        resizeMode: "contain",
        width: 80,
        height: 80,
        marginBottom: 4
    },

    contactInfo: {
        /* paddingTop: 96,*/
    },

    contactInfoItem: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: '#dedfe0',
        width: metrics.deviceWidth / 2,
        paddingTop: 30,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: 'white'
    },
    borderRight: {
        borderRightWidth: 1,
        borderRightColor: 'white'
    },

    text: {
        fontSize: 18,
        marginBottom: 18,
        textAlign: "center",

    },
    viewCenter: {
        justifyContent: "center",
        alignItems: "center",
    },

    chooseLang: {
        textAlign: "center",
        fontSize: 12,
        color: "#000000"
    },

    languages: {
        flexDirection: "row",
        justifyContent: "space-around",
        paddingHorizontal: Platform.OS == "ios" ? 110 : 80,
    },

    imageFlag: {
        width: 60,
        height: 60,
        resizeMode: 'contain'
    },

    languageContainer: {
        marginTop: 10,
    }


})