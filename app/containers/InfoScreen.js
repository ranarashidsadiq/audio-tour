import React, { Component } from 'react'
import {
    Linking, ListView, Modal, Text, TouchableOpacity, View, Image, StyleSheet, ScrollView
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import TopBar from '../components/TopBar';

import MenuScreen from "../containers/MenuScreen";

import styles from '../components/styles/doctors';

class InfoScreen extends Component {

    constructor(props) {
        super(props);


        this.state = {
            showMenu: false,
            language: ""
        }
    }


    menuDrawer = () => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    };

    componentWillMount = async () => {
        const language = await AsyncStorage.getItem("@selectedLanguage:key");
        this.setState({
            language: language
        });
    }

    openLink = () => {
        // alert('open link');
        // Linking.canOpenURL('www.proeffabriek.nl').then(supported => {
        //     if (supported) {
            // Linking.openURL('https://google.com');
              Linking.openURL('https://www.proef-fabriek.nl/');
        //     } else {
        //       console.log("Don't know how to open URI: " + this.props.url);
        //     }
        //   });
    }
    render() {
        const { isFromIntro, menuBack } = this.props;
        var topBar = <TopBar isFromIntro={false} showBack={true} showLogo={true} showMenu={true} onClick={this.menuDrawer} />
        if (this.props.isFromIntro == true) {
            topBar = <TopBar isFromIntro={true} showBack={true} showLogo={true} showMenu={false} goBack={this.props.menuBack} />
        }
        return (
            <View style={styleInfoScreen.container}>

                <View>
                    {topBar}
                </View>
                <ScrollView>
                    <View style={{ marginTop: 18 }}>
                        <Text style={[styleInfoScreen.text,
                        {
                            textAlign: "left",
                            marginLeft: 12,
                            fontWeight: "bold",
                            fontSize: 16,
                            color: "#1f7ab1"
                        }]}>Eten, drinken, proeven, beleven en genieten in De Proeffabriek </Text>
                    </View>

                    <View style={{ marginTop: 12 }}>
                        <Text style={[styleInfoScreen.text, { textAlign: "left", marginLeft: 12, marginRight: 12, fontSize: 17, color: "#5f9fc6" }]}>
                        Waar de machines van de oude veevoederfabriek nu zwijgen, laat een nieuwe CHV Noordkade van zich horen. Met een unieke mix van cultuur en food, horeca en leisure, educatie en inspiratie.  Een belangrijk deel wordt ingenomen door De Proeffabriek. Maak kennis met de smaakmakers. Stuk voor stuk ‘goed op smaak’. In deze audiotour vertellen zij hun verhaal.  Nog leuker vinden ze het om dit verhaal in het ‘echt’ aan je te vertellen en je laten je proeven van wat ze in huis hebben. In De Proeffabriek komen al je zintuigen aan bod. Je komt er ogen te kort en je smaakpapillen worden aangenaam geprikkeld.
                        </Text>
                    </View>


                    <View style={{ marginTop: 32 }}>
                        <Text style={[styleInfoScreen.text, {
                            textAlign: "left",
                            marginLeft: 12,
                            fontWeight: "bold",
                            fontSize: 16,
                            color: "#5f9fc6"
                        }]}>
                            De Proeffabriek
                    </Text>
                        <Text style={[styleInfoScreen.text, { lineHeight: 30, textAlign: "left", marginLeft: 12, fontSize: 17, color: "#5f9fc6" }]}>
                            Verlengde Noordkade 18{"\n"}
                            5462 EH Veghel{"\n"}
                        </Text>
                        <TouchableOpacity onPress={this.openLink}>
                        <Text style={[styleInfoScreen.text, { textAlign: "left", marginLeft: 12, fontSize: 17, color: "#5f9fc6", textDecorationLine: "underline" }]}>
                            www.proef-fabriek.nl
                        </Text>
                        </TouchableOpacity>
                        
                    </View>


                    <Image style={styleInfoScreen.info}
                        source={require("../components/images/Info_page_photo.jpg")}
                    />
                    <Modal
                        animationType={"slide"}
                        visible={this.state.showMenu}
                    >
                        <MenuScreen menuDrawer={this.menuDrawer} colorOrange={true} />
                    </Modal>
                </ScrollView>
            </View>
        )
    }
}

export default InfoScreen


const styleInfoScreen = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
    },
    social: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        top: 36,
        position: "absolute",
        left: 48,
        right: 48
    },
    icon: {
        resizeMode: "contain",
        width: 60,
        height: 60,
    },
    text: {
        textAlign: "center"
    },
    info: {
        height: 300,
        width: 300,
        marginTop: 18,
        alignSelf: "center",
        marginBottom: 12
    }


})