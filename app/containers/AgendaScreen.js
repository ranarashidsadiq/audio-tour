import React, { Component } from 'react'
import {
    Linking, ListView, Modal, Text, TouchableOpacity, View, Image, StyleSheet, ScrollView, ImageBackground
} from 'react-native'

import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import Permissions from 'react-native-permissions'

import email from 'react-native-email'

import call from 'react-native-phone-call'
import TopBar from '../components/TopBar';

import MenuScreen from "../containers/MenuScreen";
import { metrics, colors, constants, } from '../themes';
import AgendaItem from '../components/AgendaItem';
import WorkshopItem from '../components/WorkshopItem';

class AgendaScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMenu: false,
            language: "",
            callPermission: false,
            number: "+310413712962"
        }
    }

    menuDrawer = () => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    };

    componentWillMount = async () => {
        const language = await AsyncStorage.getItem("@selectedLanguage:key");
        this.setState({
            language: language
        });
    }
    render() {
        return (
            <View style={styleAgendaScreen.container}>
                <View >
                    <TopBar showBack={true} showLogo={true} showMenu={true} onClick={this.menuDrawer} colorOrange={this.props.colorOrange} />
                </View>
                <ScrollView>
                    <View style={styleAgendaScreen.topContainer}>
                        <ImageBackground style={{ flex: 1, justifyContent: 'flex-end' }} source={require("../components/images/Info_page_photo.jpg")} >
                            <View style={styleAgendaScreen.headerTextView}>
                                <Text style={[styleAgendaScreen.headerText]}>{this.props.navigation.state.params.screenName}</Text>
                            </View>
                            <View style={[styleAgendaScreen.subHeaderText]}>
                                <Text style={{ color: "white", padding: 10 }}>
                                    {this.props.navigation.state.params.screenName === "WORKSHOP" ?
                                        metrics.workshopDutchText :
                                        metrics.agendaDutchText}
                                </Text>
                            </View>
                        </ImageBackground>
                    </View>

                    {this.props.navigation.state.params.screenName === "WORKSHOP" ? <WorkshopItem Item={this.props.navigation.state.params.Item} comleteDescription={true} /> : <AgendaItem Item={this.props.navigation.state.params.Item} comleteDescription={true} />}
                    <Modal
                        animationType={"slide"}
                        visible={this.state.showMenu}
                    >
                        <MenuScreen menuDrawer={this.menuDrawer} colorOrange={true} />
                    </Modal>
                </ScrollView>
            </View>
        )
    }
}

export default AgendaScreen


const styleAgendaScreen = StyleSheet.create({

    topContainer: {
        flex: 1,
        alignItems: 'stretch',
        height: 290,
        width: metrics.deviceWidth,
        marginTop: 0,
    },
    topImage: {
        flexGrow: 1,
        height: null,
        width: null,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTextView: {
        marginBottom: 50
        // position: 'absolute',
        // width: metrics.deviceWidth,
        // marginTop: 0,
        // justifyContent: 'center',
    },
    headerText: {
        color: colors.white,
        textAlign: 'center',
        fontSize: 30,
    },
    subHeaderText: {

        // position: 'absolute',
        // color: colors.white,
        // width: metrics.deviceWidth,
        // marginTop: -145,
        // textAlign: 'justify',
        // lineHeight: 21,
        backgroundColor: colors.orangeTransparent,

    },


    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        alignItems: "center"
    },

    icon: {
        resizeMode: "contain",
        width: 80,
        height: 80,
        marginBottom: 4
    },

    contactInfo: {
        paddingTop: 96,
    },

    contactInfoItem: {
        justifyContent: "center",
        alignItems: "center"
    },

    text: {
        fontSize: 18,
        marginBottom: 18,
        textAlign: "center"
    },

    chooseLang: {
        textAlign: "center",
        fontSize: 12,
        color: "#000000"
    },

    // languages: {
    //     flexDirection: "row",
    //     justifyContent: "space-around",
    //     paddingHorizontal: Platform.OS == "ios" ? 110 : 80,
    // },

    imageFlag: {
        width: 60,
        height: 60,
        resizeMode: 'contain'
    },

    languageContainer: {
        position: "absolute",
        left: 20,
        bottom: 8,
        right: 20
    }


})




// import React, { Component } from 'react'
// import {
//     ListView, Modal, Text, TouchableOpacity, View, Image, ScrollView, StyleSheet, AsyncStorage, Platform
// } from 'react-native'
// import Icon from 'react-native-vector-icons/FontAwesome'

// import { setLanguage } from "../core-module/actions"

// import { withNavigation } from 'react-navigation';
// import { connect } from "react-redux";
// import { bindActionCreators } from "redux";

// import TopBar from "../components/TopBar";
// import MenuScreen from "../containers/MenuScreen";
// import { colors, metrics } from '../themes';
// import styles from '../components/styles/doctors';

// class AgendaScreen extends Component {
//     constructor(props) {
//         super(props);


//         this.state = {
//             language: ""
//         }
//     }

//     render() {

//         return (
//             <View style={styleAgendaScreen.container}>
//                 <View >
//                     <TopBar showBack={false} showLogo={true} showMenu={true} onClick={this.props.menuDrawer} colorOrange={this.props.colorOrange} />
//                 </View>


//                 <ScrollView>

//                     <Image style={styleAgendaScreen.topImage}
//                         source={require("../components/images/Info_page_photo.jpg")}
//                     />
//                     <Modal
//                         animationType={"slide"}
//                         visible={this.state.showMenu}
//                     >
//                         <MenuScreen menuDrawer={this.menuDrawer} colorOrange={true} />
//                     </Modal>
//                 </ScrollView>


//             </View>
//         )
//     }
// }

// const mapSateToProps = state => ({
//     language: state.language,
// });

// const mapDispatchToProps = dispatch => {
//     return bindActionCreators(
//         {
//             setLanguage
//         },
//         dispatch
//     );
// };

// export default connect(mapSateToProps, mapDispatchToProps)(withNavigation(AgendaScreen));

// //export default withNavigation(MenuScreen);


// const styleAgendaScreen = StyleSheet.create({

//     topImage: {
//         height: 300,
//         width: metrics.deviceWidth,
//         marginTop: 0,
//         alignSelf: "center"
//     },
//     container: {
//         flex: 1,
//         backgroundColor: "#FFFFFF",
//         alignItems: "center"
//     },

//     icon: {
//         resizeMode: "contain",
//         width: 80,
//         height: 80,
//         marginBottom: 4
//     },

//     contactInfo: {
//         paddingTop: 96,
//     },

//     contactInfoItem: {
//         justifyContent: "center",
//         alignItems: "center"
//     },

//     text: {
//         fontSize: 18,
//         marginBottom: 18,
//         textAlign: "center"
//     },

//     chooseLang: {
//         textAlign: "center",
//         fontSize: 12,
//         color: "#000000"
//     },

//     languages: {
//         flexDirection: "row",
//         justifyContent: "space-around",
//         paddingHorizontal: Platform.OS == "ios" ? 110 : 80,
//     },

//     imageFlag: {
//         width: 60,
//         height: 60,
//         resizeMode: 'contain'
//     },

//     languageContainer: {
//         position: "absolute",
//         left: 20,
//         bottom: 8,
//         right: 20
//     }


// })