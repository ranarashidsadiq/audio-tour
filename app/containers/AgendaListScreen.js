import React, { Component } from 'react'
import {
    Linking, FlatList, Modal, Text, TouchableOpacity, View, Image, StyleSheet, ScrollView
} from 'react-native'

import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import Permissions from 'react-native-permissions'

import email from 'react-native-email'

import call from 'react-native-phone-call'
import TopBar from '../components/TopBar';

import MenuScreen from "../containers/MenuScreen";
import { metrics, colors, constants, } from '../themes';
import AgendaItem from '../components/AgendaItem';
import WorkshopItem from '../components/WorkshopItem';
import Spinner from 'react-native-loading-spinner-overlay';

class AgendaListScreen extends Component {
    constructor(props) {
        super(props);


        this.state = {
            showMenu: false,
            language: "",
            callPermission: false,
            number: "+310413712962",
            visible: true,
            data:[]
        }
    }

    menuDrawer = () => {
        this.setState({
            showMenu: !this.state.showMenu,
           
        })
    };

    componentWillMount = async () => {
        
     this.loadAgendaScreen();
    }

    loadAgendaScreen(){
        const self = this;
        // alert(this.props.navigation.state.params.screenName);
        if (this.props.navigation.state.params.screenName === "WORKSHOP") {
            fetch('https://www.proef-fabriek.nl/wp-json/acf/v3/pages/20')
                .then((response) => response.json())
                .then((responseJson) => {

                    console.log(responseJson.acf.workshops);
                    self.setState({ data: responseJson.acf.workshops, visible: false });

                })
                .catch((error) => {
                    console.error(error);
                });

        } else {
            fetch('https://www.proef-fabriek.nl/wp-json/wp/v2/event')
                .then((response) => response.json())
                .then((responseJson) => {
                    self.setState({ data: responseJson, visible: false });

                })
                .catch((error) => {
                    console.error(error);
                });

        }
    }
    componentWillUnmount(){
       
    }
    componentDidMount() {
       
        Permissions.check('callPhone').then(response => {
            this.setState({ callPermission: response })
        })
    }
    _renderItem = ({ item }) => {
        if (this.props.navigation.state.params.screenName === "WORKSHOP") {
            return <WorkshopItem
                id={item.key}
                Item={item}
            />
        } else {
            return <AgendaItem
                id={item.id}
                Item={item}
            />
        }
    };


    render() {
        return (
            <View style={styleAgendaScreen.container}>
                <View >
                    <TopBar showBack={true} showLogo={true} showMenu={true} onClick={this.menuDrawer} colorOrange={this.props.colorOrange} />
                </View>
                <ScrollView>
                    <Spinner visible={this.state.visible} textContent={"Laden..."} textStyle={{ color: '#FFF' }} />
                    <FlatList
                        data={this.state.data}
                        renderItem={this._renderItem}
                    />
                </ScrollView>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.showMenu}
                    onRequestClose={this.props.showDoctorslist}
                >
                    <MenuScreen menuDrawer={this.menuDrawer} colorOrange={true}  agendaScreen ={true} loadAgendaScreen={this.loadAgendaScreen}/>
                </Modal>
            </View>
        )
    }
}

export default AgendaListScreen


const styleAgendaScreen = StyleSheet.create({

    topContainer: {
        flex: 1,
        alignItems: 'stretch',
        height: 290,
        width: metrics.deviceWidth,
        marginTop: 0,
    },
    topImage: {
        flexGrow: 1,
        height: null,
        width: null,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTextView: {
        height: 145,
        position: 'absolute',
        width: metrics.deviceWidth,
        marginTop: 0,
        justifyContent: 'center',
    },
    headerText: {
        color: colors.white,
        textAlign: 'center',
        fontSize: 30,
    },
    subHeaderText: {
        height: 145,
        position: 'absolute',
        color: colors.white,
        width: metrics.deviceWidth,
        marginTop: 145,
        textAlign: 'justify',
        lineHeight: 21,
        backgroundColor: colors.orangeTransparent,
        paddingHorizontal: 15,
        paddingVertical: 15,
    },


    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        alignItems: "center"
    },

    icon: {
        resizeMode: "contain",
        width: 80,
        height: 80,
        marginBottom: 4
    },

    contactInfo: {
        paddingTop: 96,
    },

    contactInfoItem: {
        justifyContent: "center",
        alignItems: "center"
    },

    text: {
        fontSize: 18,
        marginBottom: 18,
        textAlign: "center"
    },

    chooseLang: {
        textAlign: "center",
        fontSize: 12,
        color: "#000000"
    },

    // languages: {
    //     flexDirection: "row",
    //     justifyContent: "space-around",
    //     paddingHorizontal: Platform.OS == "ios" ? 110 : 80,
    // },

    imageFlag: {
        width: 60,
        height: 60,
        resizeMode: 'contain'
    },

    languageContainer: {
        position: "absolute",
        left: 20,
        bottom: 8,
        right: 20
    }


})




// import React, { Component } from 'react'
// import {
//     ListView, Modal, Text, TouchableOpacity, View, Image, ScrollView, StyleSheet, AsyncStorage, Platform
// } from 'react-native'
// import Icon from 'react-native-vector-icons/FontAwesome'

// import { setLanguage } from "../core-module/actions"

// import { withNavigation } from 'react-navigation';
// import { connect } from "react-redux";
// import { bindActionCreators } from "redux";

// import TopBar from "../components/TopBar";
// import MenuScreen from "../containers/MenuScreen";
// import { colors, metrics } from '../themes';
// import styles from '../components/styles/doctors';

// class AgendaScreen extends Component {
//     constructor(props) {
//         super(props);


//         this.state = {
//             language: ""
//         }
//     }

//     render() {

//         return (
//             <View style={styleAgendaScreen.container}>
//                 <View >
//                     <TopBar showBack={false} showLogo={true} showMenu={true} onClick={this.props.menuDrawer} colorOrange={this.props.colorOrange} />
//                 </View>


//                 <ScrollView>

//                     <Image style={styleAgendaScreen.topImage}
//                         source={require("../components/images/Info_page_photo.jpg")}
//                     />
//                     <Modal
//                         animationType={"slide"}
//                         visible={this.state.showMenu}
//                     >
//                         <MenuScreen menuDrawer={this.menuDrawer} colorOrange={true} />
//                     </Modal>
//                 </ScrollView>


//             </View>
//         )
//     }
// }

// const mapSateToProps = state => ({
//     language: state.language,
// });

// const mapDispatchToProps = dispatch => {
//     return bindActionCreators(
//         {
//             setLanguage
//         },
//         dispatch
//     );
// };

// export default connect(mapSateToProps, mapDispatchToProps)(withNavigation(AgendaScreen));

// //export default withNavigation(MenuScreen);


// const styleAgendaScreen = StyleSheet.create({

//     topImage: {
//         height: 300,
//         width: metrics.deviceWidth,
//         marginTop: 0,
//         alignSelf: "center"
//     },
//     container: {
//         flex: 1,
//         backgroundColor: "#FFFFFF",
//         alignItems: "center"
//     },

//     icon: {
//         resizeMode: "contain",
//         width: 80,
//         height: 80,
//         marginBottom: 4
//     },

//     contactInfo: {
//         paddingTop: 96,
//     },

//     contactInfoItem: {
//         justifyContent: "center",
//         alignItems: "center"
//     },

//     text: {
//         fontSize: 18,
//         marginBottom: 18,
//         textAlign: "center"
//     },

//     chooseLang: {
//         textAlign: "center",
//         fontSize: 12,
//         color: "#000000"
//     },

//     languages: {
//         flexDirection: "row",
//         justifyContent: "space-around",
//         paddingHorizontal: Platform.OS == "ios" ? 110 : 80,
//     },

//     imageFlag: {
//         width: 60,
//         height: 60,
//         resizeMode: 'contain'
//     },

//     languageContainer: {
//         position: "absolute",
//         left: 20,
//         bottom: 8,
//         right: 20
//     }


// })