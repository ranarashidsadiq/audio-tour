import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput, FlatList, ScrollView, Image, TouchableOpacity, Modal } from 'react-native'

import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import Permissions from 'react-native-permissions'

import TopBar from '../components/TopBar';

import MenuScreen from "../containers/MenuScreen";
import { withNavigation } from 'react-navigation';
import { metrics, colors, constants, } from '../themes';
// import PinchZoomView from 'react-native-pinch-zoom-view';
import AudioCelitem from "../components/AudioCelitem";
const doctorsData = require('../core-module/data/doctors.json');
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'





class ListFloorPlanScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMenu: false
        }

    }
    state = {
        floorPlanScreen: true,
    };
    menuDrawer = () => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    };

    componentWillMount = async () => {

    }
    toggleScreen = () => {
        // this.setState({
        //     floorPlanScreen: !this.state.floorPlanScreen
        // });
        this.props.navigation.goBack();
    }
    _renderItem = ({ item }) => {
        return <AudioCelitem
            id={item.key}
            doctor={item}
            screenName={"floorListScreen"}
            language={this.props.language}
        />
    };

    render() {

        return (
            <View>
                <View style={styleAgendaScreen.container}>
                    <View >
                        <TopBar showBack={true} showLogo={true} showMenu={true} onClick={this.menuDrawer} />
                    </View>
                    <View ><Text style={styleAgendaScreen.containerTitle}>LEGENDA</Text></View>
                    <View style={{ height: metrics.deviceHeight - 117 }}>
                        <FlatList
                            data={doctorsData}
                            renderItem={this._renderItem}
                        />
                    </View>
                    <View style={{ position: "absolute", top: metrics.deviceHeight - 100, right: 0 }}>
                        <TouchableOpacity onPress={this.toggleScreen}>
                            <Image
                                source={require('../../assests/toggleiconeWhite.png')}
                                style={{ width: 50, height: 50 }}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.showMenu}
                    onRequestClose={this.props.showDoctorslist}
                >
                    <MenuScreen menuDrawer={this.menuDrawer} colorOrange={true} />
                </Modal>
            </View>
        )


    }
}


const mapStateToProps = (state) => {
    let doctors = state.doctors.doctors;
    let language = state.language

    return {
        language: language,
    }
}
export default connect(mapStateToProps)(withNavigation(ListFloorPlanScreen));


const styleAgendaScreen = StyleSheet.create({

    topContainer: {
        flex: 1,
        alignItems: 'stretch',
        height: 290,
        width: metrics.deviceWidth,
        marginTop: 0,
    },
    container: {
        backgroundColor: colors.orange
    },
    containerTitle: {
        fontSize: 25,
        padding: 12,
        fontWeight: "bold",
        textDecorationLine: "underline",
        color: "white"
    },
    topImage: {
        flexGrow: 1,
        height: null,
        width: null,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTextView: {
        height: 145,
        position: 'absolute',
        width: metrics.deviceWidth,
        marginTop: 0,
        justifyContent: 'center',
    },
    headerText: {
        color: colors.white,
        textAlign: 'center',
        fontSize: 30,
    },
    subHeaderText: {
        height: 145,
        position: 'absolute',
        color: colors.white,
        width: metrics.deviceWidth,
        marginTop: 145,
        textAlign: 'justify',
        lineHeight: 21,
        backgroundColor: colors.orangeTransparent,
        paddingHorizontal: 15,
        paddingVertical: 15,
    },
})


