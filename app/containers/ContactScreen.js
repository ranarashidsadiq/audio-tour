import React, { Component } from 'react';
import MapView from 'react-native-maps';
import openMap from 'react-native-open-maps';

import {
    Linking,ListView, Modal, Text, TouchableOpacity, View, Image, StyleSheet, ScrollView, TouchableHighlight
} from 'react-native'

import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import Permissions from 'react-native-permissions'

import email from 'react-native-email'

import call from 'react-native-phone-call'
import TopBar from '../components/TopBar';

import MenuScreen from "../containers/MenuScreen";
import { metrics } from '../themes';

const styles = StyleSheet.create({
    container: {
        marginTop: 40,
        marginBottom: 40,
        flex: 1,
        width: metrics.deviceWidth - 50,
        alignItems: 'center',
        backgroundColor: "red"
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});

class ContactScreen extends Component {
    constructor(props) {
        super(props);


        this.state = {
            showMenu: false,
            language: "",
            callPermission: false,
            number: "+310413712962"
        }
    }

    contact = (call) => {
        // alert(call);
    }

    menuDrawer = () => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    };
    _goToYosemite() {
        openMap({ latitude: 51.6153889, longitude: 5.5267557 });
    }

    componentWillMount = async () => {
        const language = await AsyncStorage.getItem("@selectedLanguage:key");
        this.setState({
            language: language,
            coordinate: {
                latitude: 31.5444808,
                longitude: 74.3323639,
            },
        });
    }

    componentDidMount() {
        Permissions.check('callPhone').then(response => {
            this.setState({ callPermission: response })
        })
    }

    PhoneCallToUser = () => {
        const args = {
            number: this.state.number, // String value with the number to call
            prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
        }
        //    alert('call');
        call(args).catch(console.error)
        // Permissions.request('callPhone', {
        //     rationale: {
        //         title: 'Need Call Permission',
        //         message:
        //             'Needs Permission ' +
        //             'so you can make a call.',
        //     },
        // }).then(response => {
        //     this.setState({ callPermission: response })
        // })
        // if (this.state.callPermission == "authorized") {
        //     RNImmediatePhoneCall.immediatePhoneCall(this.state.number);
        // }
    }

    sendMail = () => {
        const to = ['info@proef-fabriek.nl'] // string or array of email addresses
        email(to, {
            // Optional additional arguments
            subject: '',
            body: ''
        }).catch(console.error)
    }
    openFacebook = () => {
        Linking.openURL('https://www.facebook.com/Proeffabriek');
    }

    render() {

        return (
            <View style={styleContactScreen.container}>
                <View>
                    <TopBar showBack={true} showLogo={true} showMenu={true} onClick={this.menuDrawer} fromContact={true} />
                </View>

                {/* <View style={styleContactScreen.social}>
                        <TouchableOpacity disabled={true} onPress={() => this.contact("WHATSAPP")}>
                            <Image style={styleContactScreen.icon}
                                source={require("../../assests/whatsapp.png")}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity disabled={true} onPress={() => this.contact("VIBER")}>
                            <Image style={styleContactScreen.icon}
                                source={require("../../assests/viber.png")}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity disabled={true} onPress={() => this.contact("EMAIL")}>
                            <Image style={styleContactScreen.icon}
                                source={require("../../assests/email.png")}
                            />
                        </TouchableOpacity>

                    </View> */}

                <TouchableOpacity onPress={this.PhoneCallToUser}>
                    <View style={{ justifyContent: "center" }}>
                        <Text style={[styleContactScreen.text, { color: "#e95324", fontSize: 32, marginTop: 12 }]}>+31 (0)413 712 962</Text>
                    </View>
                </TouchableOpacity>

                <View style={{ marginTop: 24 }}>
                    <Text style={[styleContactScreen.text, {
                        textAlign: "left",
                        fontWeight: "bold",
                        fontSize: 17,
                        color: "#0069A7"
                    }]}>
                        Proeffabriek Veghel
                        </Text>
                    <Text style={[styleContactScreen.text, {
                        lineHeight: 25,
                        textAlign: "left",
                        fontSize: 17,
                        color: "#5f9fc6"
                    }]}>
                        Verlengde Noordkade 18{"\n"}
                        5462 EH Veghel
                        </Text>

                    <TouchableOpacity onPress={this.sendMail}>
                        <Text style={{
                            marginTop: 24,
                            textAlign: "left",
                            fontSize: 17,
                            color: "#5f9fc6"
                        }}>
                            E. info@proef-fabriek.nl
                            </Text>
                    </TouchableOpacity>

                    <Text style={{
                        textAlign: "left",
                        fontSize: 17,
                        color: "#5f9fc6"
                    }} onPress={this.openFacebook}>
                        F. www.facebook.com/Proeffabriek
                        </Text>
                </View>
                <Modal
                    animationType={"slide"}
                    visible={this.state.showMenu}
                >
                    <MenuScreen menuDrawer={this.menuDrawer} colorOrange={true} />
                </Modal>

                <View style={styles.container}>
                    <MapView style={styles.map}
                        initialRegion={{
                            latitude: 51.6153889,
                            longitude: 5.5267557,
                            latitudeDelta: 0.02,
                            longitudeDelta: 0.02,
                        }}>

                        <MapView.Marker
                            coordinate={{
                                latitude: 51.6153889,
                                longitude: 5.5267557,
                            }}
                            title={"marker.title"}
                            description={"marker.description"}>
                            <MapView.Callout tooltip style={styles.customView}>
                                <TouchableHighlight onPress={() => this._goToYosemite()} underlayColor='#dddddd'>
                                    <View style={styles.calloutText}>
                                        <Text>{"Verlengde Noordkade 18"}</Text>
                                    </View>
                                </TouchableHighlight>
                            </MapView.Callout>
                        </MapView.Marker>

                    </MapView>
                </View>
            </View>
        )
    }
}

export default ContactScreen


const styleContactScreen = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        justifyContent: "center",
        alignItems: "center"
    },
    social: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        marginTop: 18
    },
    icon: {
        resizeMode: "contain",
        width: 60,
        height: 60,
    },
    text: {
        textAlign: "center"
    },
    options: {
        textAlign: "left",
        fontSize: 17,
        color: "#5f9fc6"
    },
    Openingstijden_options: {
        flexDirection: "row", justifyContent: "space-between"
    }


})